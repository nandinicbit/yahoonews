module.exports = function(grunt) {
    'use strict';

//    var rename = function (dest, src) { return dest + src.substring(0, src.indexOf('.min')) + '.js';};
//    var renameVersion = function (dest, src) { return dest + src.substring(0, src.indexOf('-')) + '.js'; };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            options: {
                livereload: true
            },
            assets: {
                files: [
                    'src/main/webapp/resources/**/*.js',
                    'src/main/webapp/resources/**/*.less',
                    'src/main/webapp/resources/**/*.html',
                    'src/main/webapp/WEB-INF/views/*.jsp'
                ],
                tasks: ['build', /*'jshint', 'karma:watch:run'*/]
            }
        },
//        karma: {
//            options: {
//                configFile: 'karma.conf.js'
//            },
//            unit: {
//                singleRun: true
//            },
//            phantom: {
//                singleRun: true,
//                browsers: ['PhantomJS']
//            },
//            watch: {
//                singleRun: false,
//                browsers: ['PhantomJS'],
//                background: true
//            }
//        },
//        gitinfo: {},
//        replace: {
//            dist: {
//                options: {
//                    patterns: [
//                        {
//                            match: /git.current.branch=.*/g,
//                            replacement: function () {
//                                var branch = grunt.config.data.gitinfo.local.branch;
//                                return 'git.current.branch=' + branch.current.name + ': ' + branch.current.shortSHA ;
//                            }
//                        }
//                    ]
//                },
//                files: [
//                    { expand: true, flatten: true, src: ['src/main/resources/branding.*.properties'], dest: 'src/main/resources'}
//                ]
//            }
//        },
//
//        copy: {
//            dev: {
//                files: [
//                    {expand: true, cwd: 'bower_components/angular/', src: ['angular.js'], dest: 'src/main/webapp/resources/lib/'},
//                    {expand: true, cwd: 'bower_components/angular-resource/', src: ['angular-resource.js'], dest: 'src/main/webapp/resources/lib/'},
//                    {expand: true, cwd: 'bower_components/angular-route/', src: ['angular-route.js'], dest: 'src/main/webapp/resources/lib/'},
//                    {expand: true, cwd: 'bower_components/angular-animate/', src: ['angular-animate.js'], dest: 'src/main/webapp/resources/lib/'},
//                    {expand: true, cwd: 'bower_components/angular-cookies/', src: ['angular-cookies.js'], dest: 'src/main/webapp/resources/lib/'},
//                    {expand: true, cwd: 'bower_components/angular-touch/', src: ['angular-touch.js'], dest: 'src/main/webapp/resources/lib/'},
//
//                    {expand: true, cwd: 'bower_components/bootstrap/less', src: ['*.less'], dest: 'src/main/webapp/resources/css/bootstrap/'},
//                    {expand: true, cwd: 'bower_components/less.js/dist/', src: ['less-1.7.0.js'], dest: 'src/main/webapp/resources/lib/', rename: renameVersion},
//
//                    {expand: true, cwd: 'bower_components/lodash/dist', src: ['lodash.js'], dest: 'src/main/webapp/resources/lib/'},
//                    {expand: true, cwd: 'bower_components/angular-ui-bootstrap-bower/', src: ['ui-bootstrap.js'], dest: 'src/main/webapp/resources/lib/'},
//
//                    {expand: true, cwd: 'bower_components/angulartics/src/', src: ['angulartics.js', 'angulartics-google-analytics.js'], dest: 'src/main/webapp/resources/lib/'}
//
//                ]
//            },
//            release: {
//                files: [
//                    {expand: true, cwd: 'bower_components/angular/', src: ['angular.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//                    {expand: true, cwd: 'bower_components/angular-resource/', src: ['angular-resource.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//                    {expand: true, cwd: 'bower_components/angular-route/', src: ['angular-route.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//                    {expand: true, cwd: 'bower_components/angular-animate/', src: ['angular-animate.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//                    {expand: true, cwd: 'bower_components/angular-cookies/', src: ['angular-cookies.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//                    {expand: true, cwd: 'bower_components/angular-touch/', src: ['angular-touch.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//
//                    {expand: true, cwd: 'bower_components/lodash/dist/', src: ['lodash.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//                    {expand: true, cwd: 'bower_components/momentjs/min/', src: ['moment.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//                    {expand: true, cwd: 'bower_components/less.js/dist/', src: ['less-1.7.0.min.js'], dest: 'src/main/webapp/resources/lib/', rename: renameVersion},
//                    {expand: true, cwd: 'bower_components/angular-ui-bootstrap-bower/', src: ['ui-bootstrap.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename},
//
//                    {expand: true, cwd: 'bower_components/angulartics/dist/', src: ['angulartics.min.js', 'angulartics-google-analytics.min.js'], dest: 'src/main/webapp/resources/lib/', rename: rename}
//                ]
//            }
//        },
//        shell: {
//            options: {
//                stdout: true
//            },
//            bower: {
//                command: 'bower install',
//            },
//            protractor:{
//                command: 'protractor protractor.conf.js',
//            },
//            'protractor-debug':{
//                command: 'protractor debug protractor.conf.js',
//            },
//            'protractor-sauce': {
//                command: 'protractor protractor.conf.js --baseUrl=https://ehs.ucop.edu/sop-dev --sauceUser=itsvc-tp --sauceKey=394f78b2-c49b-4dd8-9492-b27e8be32893',
//            }
//        },
//        jshint: {
//            options: {
//                jshintrc: '.jshintrc'
//            },
//            all: [
//                'Gruntfile.js',
//                'src/main/webapp/resources/scripts/{,*/}*.js',
//                'src/test/webapp/{,*/}*.js'
//            ]
//        },
        less: {
            dist: {
                files: {
                    "src/main/webapp/resources/css/build.css": "src/main/webapp/resources/css/project.less"
                }
            }
        },
        usemin: {
            html: ['src/main/webapp/WEB-INF/layout/default-layout.jsp','src/main/webapp/WEB-INF/layout/no-nav-layout.jsp','src/main/webapp/WEB-INF/layout/voucher-layout.jsp','src/main/webapp/WEB-INF/views/help.jsp','src/main/webapp/WEB-INF/views/errors/error.jsp','src/main/webapp/WEB-INF/views/errors/403.jsp'],
            options: {
              blockReplacements: {
                css: function (block) {
                    return '<link rel="stylesheet" type="text/css" href="<c:url value=\'' + block.dest + '\'/>"/>';
                }
              }
            }
        },
//        concat: {
//            dist: {
//                src: ['src/main/webapp/resources/scripts/**/*.js', 'src/main/webapp/resources/scripts/*.js'],
//                dest: 'src/main/webapp/resources/scripts/build.js'
//            }
//        },
        clean: [/*'src/main/webapp/resources/scripts/build.js',*/ 'src/main/webapp/resources/css/build.css'],
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
//    grunt.loadNpmTasks('grunt-contrib-copy');
//    grunt.loadNpmTasks('grunt-contrib-jshint');
//    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-clear');
//    grunt.loadNpmTasks('grunt-shell');
//    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-usemin');
//    grunt.loadNpmTasks('grunt-gitinfo');
//    grunt.loadNpmTasks('grunt-replace');

    grunt.registerTask('default', ['build']);
//    grunt.registerTask('package', ['shell:bower', 'copy:dev']);

    // watch
    grunt.registerTask('live', [/*'karma:watch',*/ 'watch:assets']);

    // test
//    grunt.registerTask('test:unit', ['jshint', 'karma:unit']);
//    grunt.registerTask('test:e2e', ['jshint', 'shell:protractor']);
//    grunt.registerTask('test:e2e-debug', ['jshint', 'shell:protractor-debug']);

    // build
    grunt.registerTask('build', ['clean', /*'jshint', 'gitinfo', 'replace', 'concat',*/ 'less', 'usemin']);

};