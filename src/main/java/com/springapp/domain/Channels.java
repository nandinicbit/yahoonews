package com.springapp.domain;

/**
 * Created by nparimi on 5/12/15.
 */
public enum Channels implements LabelValue{
	CNN("http://rss.cnn.com/rss/cnn_topstories.rss"),
	FOX("http://feeds.foxnews.com/foxnews/latest"),
	YAHOO("http://apps.shareholder.com/rss/rss.aspx?channels=632&companyid=YHOO&sh_auth=175476113%2E0%2E0%2E42139%2E4fd6931a077741a9383d58acbad143b0"),
	NY("http://rss.nytimes.com/services/xml/rss/nyt/World.xml");

	private final String label;

	private Channels(String label) {
		this.label = label;
	}

	@Override
	public String getValue() {
		return name();
	}

	@Override
	public String getLabel() {
		return label;
	}


}
