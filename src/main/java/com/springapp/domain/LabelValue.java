package com.springapp.domain;

public interface LabelValue {

	public abstract String getValue();

	public abstract String getLabel();

}
