package com.springapp.domain;

/**
 * Created by nparimi on 5/11/15.
 */
public class NewsFeed {
	private String heading;
	private String content;
	private String url;

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
