package com.springapp.mvc;

import com.springapp.domain.Channels;
import com.springapp.util.CnnUtil;
import com.sun.syndication.feed.synd.SyndFeed;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView Home() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("home");
		return mav;
	}

	@RequestMapping(method = RequestMethod.GET, value="home.htm")
	public ModelAndView homeNav() {
		return Home();
	}

	@RequestMapping(method = RequestMethod.GET,value="news.htm")
	public ModelAndView News(ModelAndView mav,String channel) {
		SyndFeed faqs = null;
		try {
			faqs = CnnUtil.getCnnNewsFeed(Channels.valueOf(channel).getLabel());
		} catch (Exception e) {
			e.printStackTrace();
		}
		mav.addObject("cnnFeed", CnnUtil.getFaqsFromFeed(faqs));
		mav.setViewName("news");
		return mav;
	}


}