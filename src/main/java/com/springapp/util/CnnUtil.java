package com.springapp.util;

import com.springapp.domain.NewsFeed;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by nparimi on 5/11/15.
 */
public class CnnUtil {
	private static URL cnn;


	public static List<NewsFeed> getFaqsFromFeed(SyndFeed syndFeed) {

		List<NewsFeed> feeds = new ArrayList<>();
		try {
			if(syndFeed !=null) {
				Iterator<SyndEntry> entryIterator = syndFeed.getEntries().iterator();
				while (entryIterator.hasNext()) {
					SyndEntry entry = entryIterator.next();
					NewsFeed feed = new NewsFeed();
					String syndContentString = "";
					if (entry.getDescription() != null) {
						syndContentString = entry.getDescription().getValue();
					}
					feed.setHeading(entry.getTitle());
					feed.setContent(syndContentString);
					feed.setUrl(entry.getLink());
					feeds.add(feed);
				}
			}
		} catch(Exception ex) {

		}
		return feeds;
	}

	public static SyndFeed getCnnNewsFeed(String url) throws Exception{
		try {
			if(!url.toString().isEmpty()) {
				return new SyndFeedInput().build(new XmlReader(new URL(url)));
			}
		} catch (Exception ex) {

		}
		return null;
	}
}
