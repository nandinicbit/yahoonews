<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<div class="dropdown">
				<a class="navbar-brand text-center nb-logo">News Blast</a>
			</div>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="home.htm">Home</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">News <span class="caret"></span></a>
				      <ul class="dropdown-menu">
						  <li><a href="news.htm?channel=CNN">CNN</a></li>
						  <li><a href="news.htm?channel=YAHOO">Yahoo</a></li>
						  <li><a href="news.htm?channel=FOX">Fox</a></li>
						  <li><a href="news.htm?channel=NY">NY</a></li>
					  </ul>
				</li>
			</ul>
		</div>
	</div>
</nav>