<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include.jsp"/>
<div class="main-content">
	<c:if test="${cnnFeed != null}">
		<c:forEach items="${cnnFeed}" var="feed">
			<div>
				<p><a href="${feed.url}">${feed.heading}</a></p>
				<p class="feed-content">${feed.content}</p>
			</div>
		</c:forEach>
	</c:if>
</div>
